% To test this, first compile and run the C++ code
% ./encode_sequence_pixel2uv_projection_[examplexxx]

idx = 6;

%%% Face
switch (idx)
    case 1
        texture_path = '../opengl/model/face/texture.png';
        projection_path = '~/Work/code/TSRID/data/FaceHighRes/projection/';
        real_data_path = '~/Work/code/TSRID/data/FaceHighRes/image/factor_2';
        h_img = 1920;
        w_img = 2560;
        image_indices = 0 : 88;
    
%%% TempleRing
    case 2
        texture_path = '../opengl/model/temple_ring/texture.png';
        projection_path = '~/Work/code/TSRID/data/TempleRing/projection/';
        real_data_path = '~/Work/code/TSRID/data/TempleRing/image/factor_2';
        h_img = 960;
        w_img = 1280;
        image_indices = 0 : 46;

%%% Buddha
    case 3
        texture_path = '../opengl/model/buddha/texture_low_res.png';
        projection_path = '~/Work/code/TSRID/data/BuddhaLow_17_37/projection';
        real_data_path = '~/Work/code/TSRID/data/BuddhaLow_17_37/image/factor_2';
        h_img = 1872;
        w_img = 2808;
        image_indices = 17 : 37;
   
%%% Beethoven
    case 4
        texture_path = '../opengl/model/beethoven/texture.png';
        projection_path = '~/Work/code/TSRID/data/Beethoven/projection';
        real_data_path = '~/Work/code/TSRID/data/Beethoven/image/factor_2';
        h_img = 1536;
        w_img = 2048;
        image_indices = 0 : 31;   
        
        %%% Bird
    case 5
        texture_path = '../opengl/model/bird/texture.png';
        projection_path = '~/Work/code/TSRID/data/Bird/projection';
        real_data_path = '~/Work/code/TSRID/data/Bird/image/factor_2';
        h_img = 1536;
        w_img = 2048;
        image_indices = 0 : 31;    
        
        %%% Bunny
    case 6
        texture_path = '../opengl/model/bunny/texture.png';
        projection_path = '~/Work/code/TSRID/data/Bunny/projection';
        real_data_path = '~/Work/code/TSRID/data/Bunny/image/factor_2';
        h_img = 1536;
        w_img = 2048;
        image_indices = 0 : 31;    
end

n = size(image_indices, 2);
Ps = cell(n, 1);
for i = 1 : n
    image_index = image_indices(i);
    
    atlas = im2double(imread(texture_path));

    tic;
    h_atlas = size(atlas, 1);
    w_atlas = size(atlas, 2);


    Ps{i} = DecodeProjectionFromFile(...
        sprintf('%s/map_%d.txt', projection_path, image_index), ...
        w_img, h_img, w_atlas, h_atlas);
    v_gen = Ps{i} * Mat2Vec(w_atlas, h_atlas, 3, atlas);
    toc;
    
    im_matlab = im2uint8(Vec2Mat(w_img, h_img, 3, v_gen));
    im_gl     = imread(sprintf('%s/image_%d.png', projection_path, image_index));
    im_real   = imread(sprintf('%s/image_%d.png', real_data_path, image_index));

    figure(1);
    subplot(1, 3, 1);
    imshow(im_matlab);
    title('matlab');

    subplot(1, 3, 2);
    imshow(im_gl);
    title('gl');

    subplot(1, 3, 3);
    imshow(im_real);
    title('real');
       
    figure(2);
    im_matlab_2 = imresize(im_matlab, 0.5);
    imshow(abs((im_matlab_2 - im_real)));   
    
%     imwrite(abs(im_matlab_2 - im_real), sprintf('%d_diff.png', image_index));
%     imwrite(im_matlab_2, sprintf('%d_matlab.png', image_index));
%     imwrite(im_real, sprintf('%d_real.png', image_index));

    drawnow;
end