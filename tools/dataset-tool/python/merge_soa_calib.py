from os import walk
import sys

base_path = sys.argv[1]
print 'processing ' + sys.argv[1]

all_files = next(walk(base_path))[2]
txt_files = filter(lambda x: x.startswith('calib') and x.split('.')[-1] == 'txt', all_files)
txt_files = sorted(txt_files, key=lambda x: int(x.split('.')[0][6:]))

fwe = open(base_path + '/extrinsic.txt', 'w')
fwi = open(base_path + '/intrinsic.txt', 'w')
fwe.write(str(len(txt_files)) + '\n')
fwi.write(str(len(txt_files)) + '\n')

for txt_file in txt_files:
  print 'operating ' + txt_file
  fr = open(base_path + '/' + txt_file, 'r')
  R = fr.readline().strip().split(' ')
  t = fr.readline().strip().split(' ')
  K = fr.readline()
  fr.close()    

  fwe.write(' '.join(R[0:3]) + ' ' + t[0] + '\n')
  fwe.write(' '.join(R[3:6]) + ' ' + t[1] + '\n')
  fwe.write(' '.join(R[6:9]) + ' ' + t[2] + '\n')
  fwe.write('0 0 0 1\n')
  fwi.write(K)
fwe.close()
fwi.close()
