from os import walk
from os import rename
import sys

path = sys.argv[1]
all_files = next(walk(path))[2]
for x in all_files:
    if (x.startswith('Image') and x.split('.')[1] == 'png'):
        rename(path + '/' + x, path + '/image_' + str(int(x.split('.')[0][5:])-1) + '.png')
