import cv2
import numpy as np
import sys

im_path = sys.argv[1]
im = cv2.imread(im_path)

base_path = '/'.join(im_path.split('/')[0:-1])

#im_downsample = cv2.resize(im, (0,0), fx=0.5, fy=0.5)
#im_upsample = cv2.resize(im_downsample, (0,0), fx=2, fy=2)
# cv2.imwrite('init_texture.png', im_upsample)

mask = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
mask = mask > 0
mask = 255 * mask.astype(np.uint8)

cv2.imwrite(base_path + '/mask.png', mask)
