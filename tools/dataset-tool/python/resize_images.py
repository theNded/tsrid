import cv2
from os import walk

input_path = './factor_2'
output_path = './factor_4'
all_files = next(walk(input_path))[2]
png_files = filter(lambda x: x.split('.')[-1] == 'png', all_files)

for png_file in png_files:
    im0 = cv2.imread(input_path + '/' + png_file)
    im = cv2.resize(im0, (0,0), fx=0.5, fy=0.5)
    cv2.imwrite(output_path + '/' + png_file, im)
