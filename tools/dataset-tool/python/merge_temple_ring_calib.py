from os import walk

fr = open('templeR_par.txt', 'r')
fw = open('calib.traj', 'w')

n = fr.readline()
fw.write(n)

n = int(n.strip('\n'))
# K:
# 1520.400000 0.000000 302.320000
# 0.000000 1525.900000 246.870000
# 0.000000 0.000000 1.000000
for i in xrange(0, n):
  params = fr.readline().strip('\n').split(' ')
  R = params[10:-3]
  t = params[-3:]

  fw.write(' '.join(R[0:3]) + ' ' + t[0] + '\n')
  fw.write(' '.join(R[3:6]) + ' ' + t[1] + '\n')
  fw.write(' '.join(R[6:9]) + ' ' + t[2] + '\n')
  fw.write('0 0 0 1\n')
  
fw.close()  
fr.close()

