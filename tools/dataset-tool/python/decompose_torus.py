from os import system

decomposer = '../../tools/decomposition-tool/bell2014/decompose.py'
lights = [0, 1, 2]
factors = [2, 4]
for l in lights:
    for f in factors:
        base = 'lights_' + str(l) + '/atlas/factor_' + str(f)
        input_texture = base + '/init_texture.png'
        output_albedo = base + '/init_albedo.png'
        output_shading = base + '/init_shading.png'
        system(decomposer + \
               ' ' + input_texture + \
               ' -r ' + output_albedo + \
               ' -s ' + output_shading)
        
