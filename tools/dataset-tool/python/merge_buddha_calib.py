from os import walk
base_path = './Buddha/buddha-train_cameras'

all_files = next(walk(base_path))[2]
txt_files = filter(lambda x: x.split('.')[-1] == 'cam', all_files)
txt_files = sorted(txt_files, key=lambda x: int(x.split('.')[0][5:]))

fwe = open('extrinsic.txt', 'w')
fwi = open('intrinxic.txt', 'w')
fwe.write(str(len(txt_files)) + '\n')
fwi.write(str(len(txt_files)) + '\n')

for txt_file in txt_files:
  print 'operating ' + txt_file
  fr = open(base_path + '/' + txt_file, 'r')
  extrinsic = fr.readline().strip('\n').split(' ')
  intrinsic = fr.readline().strip('\n').split(' ')
  fr.close()    

  t = extrinsic[0:3]
  R = extrinsic[3:]  
  print(t)
  print(R)
  fwe.write(' '.join(R[0:3]) + ' ' + t[0] + '\n')
  fwe.write(' '.join(R[3:6]) + ' ' + t[1] + '\n')
  fwe.write(' '.join(R[6:9]) + ' ' + t[2] + '\n')
  fwe.write('0 0 0 1\n')
  fwi.write(intrinsic[0] + '\n')
fwe.close()