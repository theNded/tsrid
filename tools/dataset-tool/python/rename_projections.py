from os import walk
from os import rename

path = './FaceSmallerSize/projection/Frame000'
all_files = next(walk(path))[2]
for x in all_files:
    if (x.startswith('Projection') and x.split('.')[1] == 'mat'):
        rename(path + '/' + x, path + '/projection_' + str(int(x.split('.')[0][-3:])-1) + '.mat')