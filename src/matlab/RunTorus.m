clear; clc;

addpath('./config');
addpath('./converter');
addpath('./operator');
addpath('./projection');
addpath('./solver');

ConfigTorus;

for light = lights
    for factor = factors
        
    fprintf('Config: lights_%d, factor_%d\n', light, factor);
    input_path = sprintf('%s/lights_%d', input_base_path, light);

    output_path = sprintf('%s/lights_%d', output_base_path, light);
    system(sprintf('mkdir %s', output_path));

    for a = lambda_A
        for s = lambda_S
        RunDatasetWithParams(...
            input_path, output_path, ...
            radius, elevation, azimuth, ...
            W_atlas, H_atlas, W_img, H_img, factor, ...
            a, s, lambda_F, ...
            outer_loop, inner_loop_flow, inner_loop_atlas, ...
            sigma_A, tau_A, sigma_S, tau_S, sigma_F, tau_F, ...
            alpha, theta, ...
            log_interval);
        end    
    end
    
    end
end

function RunDatasetWithParams(...
    input_path, output_path, ...                         % Paths
    radius, elevation, azimuth, ...                      % Selected images
    W_atlas, H_atlas, W_img, H_img, factor, ...          % Resolutions
    lambda_A, lambda_S, lambda_F, ...                    % Lambdas
    outer_loop, inner_loop_flow, inner_loop_atlas, ...   % Loops
    sigma_A, tau_A, sigma_S, tau_S, sigma_F, tau_F, ...  % Meta parameters
    alpha, theta, ...
    log_interval) 

output_path = sprintf('%s/A_%f_S_%f', output_path, lambda_A, lambda_S);
system(sprintf('mkdir %s', output_path));

%% Resolutions
w_atlas = W_atlas / factor;
h_atlas = H_atlas / factor;
w_img = W_img / factor;
h_img = H_img / factor;


%% Prepare data
% Projection matrices high-res-atlas -> high-res-img
n = size(elevation, 2) * size(azimuth, 2);
Ps = cell(n, 1);
Is = cell(n, 1); 

% Ground truth
str = sprintf('%s/atlas/gt/gt_albedo.png', input_path);
Agt = im2double(imread(str));

str = sprintf('%s/atlas/gt/gt_shading.png', input_path);
Sgt = im2double(imread(str));
Sgt = Sgt(:,:,1);

str = sprintf('%s/atlas/gt/gt_texture.png', input_path);
Tgt = im2double(imread(str));

% Albedo
str = sprintf('%s/atlas/factor_%d/init_albedo.png', input_path, factor);
A = im2double(imread(str));%randn(W*H, 3);
A = ConvertMat2Vec(W_atlas, H_atlas, 3, A);
A_bar = A;
p_A  = zeros(2*W_atlas*H_atlas, 3);
q_As = cell(n, 1);

% Shading
str = sprintf('%s/atlas/factor_%d/init_shading.png', input_path, factor);
S = im2double(imread(str));%randn(W*H, 1);
S = S(:);
S_bar = S;
p_S  = zeros(2*W_atlas*H_atlas, 1);
q_Ss = cell(n, 1);

% Optical Flow
Us   = cell(n, 1);
U_bars = cell(n, 1);
p_Us = cell(n, 1);
q_Us = cell(n, 1);

Ws   = cell(n, 1);

fprintf('Initializing\n');

iter = 1;
for i = radius
for j = elevation
for k = azimuth    
    fprintf('Loading projection %d ...\n', iter);
    str_idx = sprintf('radius_%d_elevation_%d_azimuth_%d', i, j, k);
    str = sprintf('%s/projection/map_%s.txt', input_path, str_idx);
    fid = fopen(str);
    c = textscan(fid, '%d %d %f %f');
    fclose(fid);
    is = double(c{1}); js = double(c{2});
    us = c{3}; vs = c{4};
    Ps{iter} = DecodeProjection(is, js, W_img, H_img, us, vs, W_atlas, H_atlas);

    fprintf('Loading image %d ...\n', iter);
    str_idx = sprintf('radius_%d_elevation_%d_azimuth_%d', i, j, k);
    str = sprintf('%s/image/factor_%d/image_%s.png', input_path, factor, str_idx);
    Is{iter} = reshape(im2double(imread(str)), w_img*h_img, 3);
    
    % Albedo
    q_As{iter} = zeros(w_img*h_img, 3);
    
    % Shading
    q_Ss{iter} = zeros(w_img*h_img, 3);
    
    % Optical Flow
    Us{iter} = zeros(2*W_img*H_img,1);    
    U_bars{iter} = zeros(2*W_img*H_img, 1);
    p_Us{iter} = zeros(4*W_img*H_img, 1);
    q_Us{iter} = zeros(w_img*h_img, 3);
    Ws{iter} = ConvertFlowVec2Mat(W_img, H_img, Us{iter});
    
    iter = iter + 1;
end
end
end

%% Const variables
D = OperatorSubsample(W_img, H_img, 2);
K = OperatorGaussian(W_img, H_img, 3, sqrt(0.1));

mask_atlas = ones(W_atlas*H_atlas, 1);
G_atlas = OperatorGradient(W_atlas, H_atlas, mask_atlas);

mask_img = ones(H_img*W_img,1);
G_img = OperatorGradient(W_img, H_img, mask_img);

huber = @(x) ((x.^2/alpha) .* (abs(x) <= alpha)...
            + (abs(x)-alpha/2) .* (abs(x) > alpha));

%% Initialize optimizers
fprintf('Building shading solver\n');
solver_S = SolverShading();
solver_S.Init(D, K, G_atlas, W_atlas, H_atlas, mask_atlas, ...                      
              n, w_img, h_img, ...              
              lambda_S, sigma_S, tau_S, theta, alpha, ...
              false);
solver_S.Update(A, Ws, Ps, Is);          
          
fprintf('Building albedo solver\n');
solver_A = SolverAlbedo();
solver_A.Init(D, K, G_atlas, W_atlas, H_atlas, mask_atlas, ...                      
              n, w_img, h_img, ...              
              lambda_A, sigma_A, tau_A, theta, ...
              false);
solver_A.Update(S, Ws, Ps, Is);          

fprintf('Building flow solver\n');
solver_F = SolverFlow;
solver_F.Init(D, K, G_img, W_img, H_img, mask_atlas, ...                      
              w_img, h_img, ...
              lambda_F, sigma_F, tau_F, theta, ...
              false);

err_flow  = [];
err_atlas = [];

err_ds = [];
err_as = [];
err_ss = [];
err_ws = [];

psnr_shading = [];
mse_shading  = [];
ssim_shading = [];

psnr_albedo = [];
mse_albedo  = [];
ssim_albedo = [];

psnr_texture = [];
mse_texture = [];
ssim_texture = [];

for viewpoint = 1 : outer_loop
    fprintf('OUTER LOOP: %d\n', viewpoint);
        
    %% Optimize Optical flow    
    for iter_flow = 1 : inner_loop_flow
        compute_error = mod(iter_flow, log_interval) == 1;
        err_d = 0;
        err_w = 0;
        rate = 0;
        for k = 1 : n
            tic;
            fprintf('Flow estimation: viewpoint%d\n', k);
            solver_F.Update(A, S, Ps{k}, Is{k});
            [Us{k}, U_bars{k}, p_Us{k}, q_Us{k}, rate_k] ...
                = solver_F.Iterate(Us{k}, U_bars{k}, p_Us{k}, q_Us{k});
            rate = max(rate, rate_k);            
            if compute_error
                [err, err_dk, err_wk] = solver_F.ComputeEnergy(Us{k});
                err_d = err_d + err_dk;
                err_w = err_w + err_wk;
            end
            toc;
        end
        fprintf('Iteration %d: Flow change rate: %f\n', iter_flow, rate);           
        if compute_error
            err_flow = [err_flow, err_d + err_w/lambda_F];
            err_ws   = [err_ws, err_w];
            h3 = figure(3);            
            title('Error flow');
            yyaxis left; plot(err_flow); 
            yyaxis right; plot(err_ws);
            legend('total', 'flow'); drawnow;
            savefig(h3, sprintf('%s/error-flow.fig', output_path));
        end
    end
    for k = 1 : n
        Ws{k} = ConvertFlowVec2Mat(W_img, H_img, Us{k});
    end
    
    %% Optimize Atlas
    for iter_atlas = 1 : inner_loop_atlas
        compute_error = mod(iter_atlas, log_interval) == 1;

        % Optimize Shading
        tic;        
        solver_S.Update(A, Ws, Ps, Is);
        [S, S_bar, p_S, q_Ss, rate_S] = solver_S.Iterate(S, S_bar, p_S, q_Ss);
        fprintf('Iteration %d: S change rate: %f\n', iter_atlas, rate_S);        
        if compute_error
            [err, err_d, err_s] = solver_S.ComputeEnergy(S);
        end
        toc;
        im_S = solver_S.Atlas(S);

        % Optimize Albedo
        tic;
        solver_A.Update(S, Ws, Ps, Is);
        [A, A_bar, p_A, q_As, rate_A] = solver_A.Iterate(A, A_bar, p_A, q_As);
        fprintf('Iteration %d: A change rate: %f\n', iter_atlas, rate_A);
        if compute_error
            [err, err_d, err_a] = solver_A.ComputeEnergy(A);
        end
        toc;
        im_A = solver_A.Atlas(A);

        % Display and output results
        figure(1); subplot(1, 2, 1);
        imshow(im_S); drawnow;    

        figure(1); subplot(1, 2, 2);
        imshow(im_A); drawnow;
        
        if compute_error
            err_atlas = [err_atlas, err_d + err_a / lambda_A + err_s / lambda_S];
            err_ds = [err_ds, err_d];
            err_as = [err_as, err_a];
            err_ss = [err_ss, err_s];    
            psnr_albedo = [psnr_albedo, psnr(im_A, Agt)];
            mse_albedo  = [mse_albedo,  immse(im_A, Agt)];
            ssim_albedo = [ssim_albedo, ssim(im_A, Agt)];

            psnr_shading = [psnr_shading, psnr(im_S, Sgt)];
            mse_shading = [mse_shading, immse(im_S, Sgt)];
            ssim_shading = [ssim_shading, ssim(im_S, Sgt)];

            psnr_texture = [psnr_texture, psnr(im_A.*im_S, Tgt)];
            mse_texture  = [mse_texture,  immse(im_A.*im_S, Tgt)];
            ssim_texture = [ssim_texture, ssim(im_A.*im_S, Tgt)];    

            %% Primal energy
            x = 1:size(err_atlas, 2);
            h2 = figure(2);
            title('Error Atlas'); 
            yyaxis left;
            plot(x, err_atlas);
            yyaxis right;
            plot(x, err_ds, x, err_as/lambda_A, x, err_ss/lambda_S); 
            legend('total', 'data', 'albedo', 'shading');
            drawnow;
            savefig(h2, sprintf('%s/error-atlas.fig', output_path));
            
            %% PSNR, MSE, and SSIM
            h3 = figure(3);
            title('psnr albedo');
            yyaxis left;
            plot(x, psnr_albedo);
            yyaxis right;
            plot(x, mse_albedo, x, ssim_albedo); 
            legend('psnr', 'mse', 'ssim');
            drawnow;    
            savefig(h3, sprintf('%s/psnr_albedo.fig', output_path));

            h4 = figure(4);
            title('psnr shading');
            yyaxis left;
            plot(x, psnr_shading);
            yyaxis right;
            plot(x, mse_shading, x, ssim_shading); 
            legend('psnr', 'mse', 'ssim');
            drawnow;   
            savefig(h4, sprintf('%s/psnr_shading.fig', output_path));


            h5 = figure(5);
            title('psnr texture');
            yyaxis left;
            plot(x, psnr_texture);
            yyaxis right;
            plot(x, mse_texture, x, ssim_texture); 
            legend('psnr', 'mse', 'ssim');
            drawnow;      
            savefig(h5, sprintf('%s/psnr_texture.fig', output_path));

            %% Atlas images
            imwrite(im_S, sprintf('%s/shading%d-%d.png', output_path, ...
                                  viewpoint, iter_atlas));
            imwrite(im_A, sprintf('%s/albedo%d-%d.png', output_path, ...
                                  viewpoint, iter_atlas));
            imwrite(im_S.*im_A, sprintf('%s/texture%d-%d.png', output_path, ...
                                  viewpoint, iter_atlas));

            save(sprintf('%s/error.mat', output_path), ...
                'err_flow', 'err_atlas', ...
                'err_ds', 'err_as', 'err_ss', 'err_ws');
        end
    end
end
end
