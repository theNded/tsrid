classdef ProjectionMatrix < handle
    %UNTITLED8 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties ( GetAccess = 'public', SetAccess = 'private' )
         m_ProjectionMatrix;
        m_Rows;
        m_Columns;
        
        
    end
    
    methods( Access = 'public' )
        function Obj = ProjectionMatrix(varargin)
            if nargin==0
                %% Consturct a dummy matrix
                Obj.m_ProjectionMatrix = Projection();
                Obj.m_Rows = 0;
                Obj.m_Columns = 0;
                %         end
                %%%%
                %         function load(Obj, ThePreOptImageMatrix)
            elseif nargin==2
                %% Load the matrix
                ThePreOptImageMatrix = varargin{1};
                TheMemoryFlag = varargin{2};
                 %% Consturct a dummy matrix
                Obj.m_ProjectionMatrix = Projection();
                Obj.m_Rows = 0;
                Obj.m_Columns = 0;
                for ii=1:ThePreOptImageMatrix.getNbRows()
                    for jj=1:ThePreOptImageMatrix.getNbColumns()
                        
                        Texel2PixelMatFileName = ThePreOptImageMatrix.get(ii,jj).getFileName.getFullNameForOutPutTexelProjection();
                        TheHeight = ThePreOptImageMatrix.get(ii,jj).getParam().getImHeight();
                        TheWidth = ThePreOptImageMatrix.get(ii,jj).getParam().getImWidth();
                        TheChannels = ThePreOptImageMatrix.get(ii,jj).getParam().getImThirdDim();
                        TheImageSize = [TheHeight TheWidth TheChannels];
                        
                        TheHeight = ThePreOptImageMatrix.get(ii,jj).getParam().getTexHeight();
                        TheWidth = ThePreOptImageMatrix.get(ii,jj).getParam().getTexWidth();
                        TheChannels = ThePreOptImageMatrix.get(ii,jj).getParam().getTexThirdDim();
                        TheTexSize = [TheHeight TheWidth TheChannels];
                        
                        
                        TheProjection = Projection(Texel2PixelMatFileName, TheTexSize, TheImageSize, TheMemoryFlag);
                        %                         Obj.m_ProjectionMatrix(ii,jj) = TheProjection ;
                        Obj.addProjection(TheProjection,ii,jj);
                        %                     Obj.m_Rows =  max(Obj.m_Rows, ii);
                        %                     Obj.m_Columns = max(Obj.m_Columns, jj);
                    end
                end
            else
                error('ProjectionMatrix: Wrong number of Input Arguments')
            end
            
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function saveProjection(Obj, ThePreOptImageMatrix, TheMemoryFlag , TheFullPathName, TheIndexTimeFrames,ThesIndexViewPoints )
            
            %% Consturct a dummy matrix
            Obj.m_ProjectionMatrix = Projection();
            Obj.m_Rows = 0;
            Obj.m_Columns = 0;
            for ii=1:ThePreOptImageMatrix.getNbRows()
                for jj=1:ThePreOptImageMatrix.getNbColumns()
                    
                    
                    
                    Texel2PixelMatFileName = ThePreOptImageMatrix.get(ii,jj).getFileName.getFullNameForOutPutTexelProjection();
                    TheHeight = ThePreOptImageMatrix.get(ii,jj).getParam().getImHeight();
                    TheWidth = ThePreOptImageMatrix.get(ii,jj).getParam().getImWidth();
                    TheChannels = ThePreOptImageMatrix.get(ii,jj).getParam().getImThirdDim();
                    TheImageSize = [TheHeight TheWidth TheChannels];
                    
                    TheHeight = ThePreOptImageMatrix.get(ii,jj).getParam().getTexHeight();
                    TheWidth = ThePreOptImageMatrix.get(ii,jj).getParam().getTexWidth();
                    TheChannels = ThePreOptImageMatrix.get(ii,jj).getParam().getTexThirdDim();
                    TheTexSize = [TheHeight TheWidth TheChannels];
                    
                      foldername = sprintf('%s%s%03d%s',TheFullPathName,'/Projection/Frame',TheIndexTimeFrames(ii),'/');
                    if ~isdir(foldername)
                        
                        mkdir(foldername);
                    end                   
                    sfilename = sprintf('%s%s%03d%s',foldername,'/Projection_',ThesIndexViewPoints(jj),'.mat');
                    if ~logical(exist(sfilename))
                        
                        
                        
                        TheProjection = Projection(Texel2PixelMatFileName, TheTexSize, TheImageSize, TheMemoryFlag);
                        %                         Obj.m_ProjectionMatrix(ii,jj) = TheProjection ;
                        %                         Obj.addProjection(TheProjection,ii,jj);
                        %                     Obj.m_Rows =  max(Obj.m_Rows, ii);
                        %                     Obj.m_Columns = max(Obj.m_Columns, jj);
                        save(sfilename, 'TheProjection','-v7.3');
                            %%%%%%%%%%%%%%%%%%%% We do not need to save the
                        %%%%%%%%%%%%%%%%%%%% back projection matrice!
% % % %                         BackProjectionData = TheProjection.getBackProjectionMatrix();
% % % %                     
% % % %                         sfilename = sprintf('%s%s%03d%s',foldername,'/BackProjection_',ThesIndexViewPoints(jj),'.mat');
% % % %                         save(sfilename, 'BackProjectionData','-v7.3');
                    end
                    
                    
                end
            end
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function TheProjection = readProjection(Obj,  TheRowIndex,TheColumnIndex, TheFullPathName)
%             TheFullPathName = Obj.getOutPutPathName();
            foldername = sprintf('%s%s%03d%s',TheFullPathName,'/Projection/Frame', TheRowIndex,'/');
%             if ~isdir(strcat(TheFullPathName,'/Projection/'))
%                 mkdir();
%             end
          sfilename = sprintf('%s%s%03d%s',foldername,'/Projection_',TheColumnIndex,'.mat');
            load(sfilename);
            
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function   BackProjectionData = readBackProjection(Obj,  TheRowIndex,TheColumnIndex, TheFullPathName)
%             TheFullPathName = Obj.getOutPutPathName();
            foldername = sprintf('%s%s%03d%s',TheFullPathName,'/Projection/Frame', TheRowIndex,'/');
%             if ~isdir(strcat(TheFullPathName,'/Projection/'))
%                 mkdir();
%             end
          sfilename = sprintf('%s%s%03d%s',foldername,'/BackProjection_',TheColumnIndex,'.mat');
           if ~logical(exist(sfilename))
               %% If it does nto exist then check if the Projection_ exists.
               sfilename =  sprintf('%s%s%03d%s',foldername,'/Projection_',TheColumnIndex,'.mat');
               if logical(exist(sfilename))
                   load(sfilename);
                   BackProjectionData = TheProjection.getBackProjectionMatrix();
               end
           else
            load(sfilename);
           end
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function addProjection(Obj, TheProjection,  index_Rows, index_Columns)
            Obj.m_ProjectionMatrix(index_Rows, index_Columns) = TheProjection;
            Obj.m_Rows =  max(Obj.m_Rows, index_Rows);
            Obj.m_Columns = max(Obj.m_Columns, index_Columns);
        end
        % Accessors
        function NSize = getSize(Obj)
            NSize = [Obj.m_Rows Obj.m_Columns];
        end
        
        function NbRows = getNbRows(Obj)
            NbRows = Obj.m_Rows;
        end
        
        function NbColumns = getNbColumns(Obj)
            NbColumns = Obj.m_Columns;
        end
        
        function TheProjection = get(Obj, TheRowIndex,TheColumnIndex)
            TheProjection = Obj.m_ProjectionMatrix(TheRowIndex,TheColumnIndex);
        end
        
        
        
        function TotalProjectionMatrixSize = getProjectionMatSize(Obj)
             TotalProjectionMatrixSize = zeros(Obj.getNbRows(),Obj.getNbColumns(),3 );
            for ii=1:Obj.getNbRows()
                for jj=1:Obj.getNbColumns()
                    TotalProjectionMatrixSize(ii,jj,:) = Obj.get(ii,jj).getProjectionMatSize();
                end
            end
        end
        
        %% Take the projection cell array for all the projection matrices
        % that are stored in the ProjecitonMatrix object.
        function Pr = getPrMatrix(Obj,varargin)
            % Take all the projection objects from the ProjectionMatrix
            if nargin==1
                Pr = cell(Obj.getNbRows(), Obj.getNbRows());
                for ii=1:Obj.getNbRows()
                    for jj=1:Obj.getNbColumns()
                        Pr{ii,jj} = Obj.get(ii,jj).getProjectionMatrix();
                    end
                end
            elseif nargin==3
                IndexRows = varargin{1};
                IndexColumns = varargin{2};
                Pr = Obj.get(IndexRows, IndexColumns).getProjectionMatrix();
            else
                error('Projection Matrix: getPojectionMatrix Wrong Input Number of Arguments');
            end
        end
        
%       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function TransposePr = getBackPrMatrix(Obj,varargin)
            if nargin==1
                TransposePr = cell(Obj.getNbRows(), Obj.getNbRows());
                for ii=1:Obj.getNbRows()
                    for jj=1:Obj.getNbColumns()
                        TransposePr {ii,jj} = Obj.get(ii,jj).getBackProjectionMatrix();
                    end
                end
            elseif nargin==3
                IndexRows = varargin{1};
                IndexColumns = varargin{2};
                TransposePr = Obj.get(IndexRows, IndexColumns).getBackProjectionMatrix();
            else
                fprintf('Projection Magrix: getPojectionMatrix Wrong Input Number of Arguments');
            end
        end
        
        
%         
%         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         function AngleWeightsMat = VertWeightCam(Obj, texel2pixel, texsize, imsize)
% %              Texel2PointWeight
%             
%             texheight = texsize(1);
%             texwidth = texsize(2);
%             
%             imheight = imsize(1);
%             
%             imwidth = imsize(2);
%             
%             % For memory issuew, don't create new copies
%             %texelind = Texel2PointWeight(:,1);
%             %pixelind = Texel2PointWeight(:,2);
%             %angleweights = Texel2PointWeight(:,3); %% The angleweights have been already renormalized
%             
%             
%             % in Run ImageCoordinatesTexWeight
%             % angleweights = angleweights*2-1;
%             
%             % whos
%             
%             AngleWeightsMat = sparse(texel2pixel.PixelsLinearInd, texel2pixel.Texels, texel2pixel.PdfWeights,  imsize(1)* imsize(2) ,texsize(1)*texsize(2));
%             
%         end
        
    end
end

