classdef Projection < handle
    %UNTITLED8 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties ( GetAccess = 'public', SetAccess = 'private' )
        m_ProjectionData;
        m_BackProjectionData;
        m_Rows;
        m_Columns;
        
        
    end
    
    methods( Access = 'public' )
        function Obj = Projection(varargin)
            if nargin==0
                %%Dummy constructor
                Obj.m_ProjectionData = [];
                Obj.m_BackProjectionData = [];
                Obj.m_Rows = 0;
                Obj.m_Columns = 0;
            elseif nargin==4
                FileNamePath = varargin{1};
                TheTexSize = varargin{2};
                TheImageSize = varargin{3};
                MemoryFlag = varargin{4};
                %% Try to laod the field of the struct
                temp = load(FileNamePath);
                myfield = fieldnames(temp);
                texel2pixel = getfield(temp,myfield{numel(myfield)});
                
                Obj.m_ProjectionData = Obj.VertWeightCam(texel2pixel, TheTexSize,...
                    TheImageSize);
                if MemoryFlag
                    Obj.m_BackProjectionData = [];
                else
                    Obj.m_BackProjectionData = Obj.m_ProjectionData';
                end
                Obj.m_Rows = TheImageSize(1) * TheImageSize(2);
                Obj.m_Columns = TheTexSize(1) * TheTexSize(2);
                 elseif nargin==5
                     texel2pixel = varargin{1};
                TheTexSize = varargin{2};
                TheImageSize = varargin{3};
                MemoryFlag = varargin{4};
                PatchFlag = varargin{5}; % This is a flag that tells us that we compute Projection Matrix for
                % texture patch
                 Obj.m_ProjectionData = Obj.VertWeightCam(texel2pixel, TheTexSize,...
                    TheImageSize);
                if MemoryFlag
                    Obj.m_BackProjectionData = [];
                else
                    Obj.m_BackProjectionData = Obj.m_ProjectionData';
                end
                Obj.m_Rows = TheImageSize(1) * TheImageSize(2);
                Obj.m_Columns = TheTexSize(1) * TheTexSize(2);
                
                
            else
               error('Projeciton: Wrong Number of Input Arguments')
            end
            
        end
        
        %% Accessors
        function Pr = getProjectionMatrix(Obj)
            Pr = Obj.m_ProjectionData;
        end
        
        function TransposePr = getBackProjectionMatrix(Obj)
            
            if isempty(Obj.m_BackProjectionData)
                TransposePr = Obj.m_ProjectionData';
            else
                TransposePr = Obj.m_BackProjectionData;
            end
            
        end
        
        function ProjectionSize = getProjectionMatSize(Obj)
            ProjectionSize = size(Obj.m_ProjectionData);
        end
        
        function load(Obj, TheProjectionMatrix, MemoryFlag)
            Obj.m_ProjectionData = TheProjectionMatrix;
            if MemoryFlag
                Obj.m_BackProjectionData = [];
            else
                Obj.m_BackProjectionData = Obj.m_ProjectionData';
            end
            Obj.m_Rows = size(TheProjectionMatrix,1);
            Obj.m_Columns = size(TheProjectionMatrix,2);
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function AngleWeightsMat = VertWeightCam(Obj, texel2pixel, texsize, imsize)
%              Texel2PointWeight
            
            texheight = texsize(1);
            texwidth = texsize(2);
            
            imheight = imsize(1);
            
            imwidth = imsize(2);
            
            % For memory issuew, don't create new copies
            %texelind = Texel2PointWeight(:,1);
            %pixelind = Texel2PointWeight(:,2);
            %angleweights = Texel2PointWeight(:,3); %% The angleweights have been already renormalized
            
            
            % in Run ImageCoordinatesTexWeight
            % angleweights = angleweights*2-1;
            
            % whos
            
            AngleWeightsMat = sparse(texel2pixel.PixelsLinearInd, texel2pixel.Texels, texel2pixel.PdfWeights,  imsize(1)* imsize(2) ,texsize(1)*texsize(2));
            
        end
        
    end
end

