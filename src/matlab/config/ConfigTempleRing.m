%%% CONFIGURATIONS %%%
%%%%%%%%%%
% I. Paths
input_path = '../../data/TempleRing';
output_path = 'result/TempleRing';
system(sprintf('mkdir %s', output_path));

%%%%%%%%%%
% II. Resolutions
W_atlas = 1280; 
H_atlas = 960;
W_img = 1280;
H_img = 960;
factor = 2;

%%%%%%%%%%
% III. Select images (viewpoints)
% USE STEP FOR NAIVE SELECTION %
step = 1;
image_indices = 0 : step : 46;
% OR SET IT MANUALLY, e.g. %
% image_indices = [1, 5, 9, 24, 36];

%%%%%%%%%%
% IV. Loops
outer_loop = 3;
inner_loop_flow = 101;
inner_loop_atlas = 401;

%%%%%%%%%%
% V. Weights, note they are inverses
% E = E_d + (1/lamda_A) E_A + (1/lambda_S) E_S + (1/lambda_F) E_F
% TO ENABLE GRID SEARCH, EXTEND THESE TWO ARRAYS %
lambda_A = [1.5];
lambda_S = [1];
% In most cases do NOT change this parameter
lambda_F = 2000;

%%%%%%%%%%
%%% VI. Primal dual meta-parameters
%%% Step sizes
sigma_A = 0.1;
tau_A   = 0.1;
sigma_S = 0.1;
tau_S   = 0.1;
sigma_F = 0.05;
tau_F   = 0.05;
%%% Huber norm
alpha = 0.1;
%%% Extrapolation, DO NOT CHANGE
theta = 1;

%%%%%%%%%%
%%% VII. Misc
% plot error ever 10 frames
log_interval = 10;