clear; clc;

addpath('./config');
addpath('./converter');
addpath('./operator');
addpath('./projection');
addpath('./solver');

ConfigFaceLowRes;
        
%%% main program:
%%% If lambda_A and lambda_S are extended to arrays, 
%%% this would be a grid search
for a = lambda_A
    for s = lambda_S
        RunDatasetWithParams(...
            input_path, output_path, ...
            image_indices, ...
            W_atlas, H_atlas, W_img, H_img, factor, ...
            a, s, lambda_F, ...
            outer_loop, inner_loop_flow, inner_loop_atlas, ...
            sigma_A, tau_A, sigma_S, tau_S, sigma_F, tau_F, ...
            alpha, theta, ...
            log_interval);
    end
end

function RunDatasetWithParams(...
    input_path, output_path, ...                         % Paths
    image_indices, ...                                   % Selected images
    W_atlas, H_atlas, W_img, H_img, factor, ...          % Resolutions
    lambda_A, lambda_S, lambda_F, ...                    % Lambdas
    outer_loop, inner_loop_flow, inner_loop_atlas, ...   % Loops
    sigma_A, tau_A, sigma_S, tau_S, sigma_F, tau_F, ...  % Meta parameters
    alpha, theta, ...
    log_interval) 

output_path = sprintf('%s/A_%f_S_%f', output_path, lambda_A, lambda_S);
system(sprintf('mkdir %s', output_path));

%% Resolutions
w_atlas = W_atlas / factor;
h_atlas = H_atlas / factor;
w_img = W_img / factor;
h_img = H_img / factor;


%% Prepare data
% Projection matrices high-res-atlas -> high-res-img
n = size(image_indices, 2);
Ps = cell(n, 1);
Is = cell(n, 1); 

% Albedo
str = sprintf('%s/atlas/factor_%d/init_albedo.png', input_path, factor);
A = im2double(imread(str));%randn(W*H, 3);
A = ConvertMat2Vec(W_atlas, H_atlas, 3, A);
A_bar = A;
p_A  = zeros(2*W_atlas*H_atlas, 3);
q_As = cell(n, 1);

% Shading
str = sprintf('%s/atlas/factor_%d/init_shading.png', input_path, factor);
S = im2double(imread(str));%randn(W*H, 1);
S = S(:);
S_bar = S;
p_S  = zeros(2*W_atlas*H_atlas, 1);
q_Ss = cell(n, 1);

% Optical Flow
Us   = cell(n, 1);
U_bars = cell(n, 1);
p_Us = cell(n, 1);
q_Us = cell(n, 1);

Ws   = cell(n, 1);

fprintf('Initializing\n');
for i = 1 : n
    viewpoint = image_indices(i);       
    %%%%%%%%%% ONLY THIS PART IS DIFFERENT %%%%%%%%%%
    str = sprintf('%s/projection/projection_%d.mat', input_path, viewpoint);
    load (str);
    Ps{i} = TheProjection.m_ProjectionData;
    PsNormalizer = sparse(1:W_img*H_img, 1:W_atlas*H_atlas, 1./sum(Ps{i}, 2));
    Ps{i} = PsNormalizer * Ps{i};

    fprintf('Loading image %d ...\n', viewpoint);
    str = sprintf('%s/image/factor_%d/image_%d.png', input_path, factor, viewpoint);
    Is{i} = reshape(im2double(imread(str)), w_img*h_img, 3);
    
    % Albedo
    q_As{i} = zeros(w_img*h_img, 3);
    
    % Shading
    q_Ss{i} = zeros(w_img*h_img, 3);
    
    % Optical Flow
    Us{i} = zeros(2*W_img*H_img,1);    
    U_bars{i} = zeros(2*W_img*H_img, 1);
    p_Us{i} = zeros(4*W_img*H_img, 1);
    q_Us{i} = zeros(w_img*h_img, 3);
    Ws{i} = ConvertFlowVec2Mat(W_img, H_img, Us{i});
end

%% Const variables
D = OperatorSubsample(W_img, H_img, 2);
K = OperatorGaussian(W_img, H_img, 3, sqrt(0.1));

str = sprintf('%s/atlas/factor_%d/mask.png', input_path, factor);
mask_atlas = imread(str) > 0;
mask_atlas = mask_atlas(:,:,1);
mask_atlas = mask_atlas(:);

G_atlas = OperatorGradient(W_atlas, H_atlas, mask_atlas);

mask_img = ones(H_img*W_img,1);
G_img = OperatorGradient(W_img, H_img, mask_img);

huber = @(x) ((x.^2/alpha) .* (abs(x) <= alpha)...
            + (abs(x)-alpha/2) .* (abs(x) > alpha));

%% Initialize optimizers
fprintf('Building shading solver\n');
solver_S = SolverShading();
solver_S.Init(D, K, G_atlas, W_atlas, H_atlas, mask_atlas, ...                      
              n, w_img, h_img, ...              
              lambda_S, sigma_S, tau_S, theta, alpha, ...
              false);
solver_S.Update(A, Ws, Ps, Is);          
          
fprintf('Building albedo solver\n');
solver_A = SolverAlbedo();
solver_A.Init(D, K, G_atlas, W_atlas, H_atlas, mask_atlas, ...                      
              n, w_img, h_img, ...              
              lambda_A, sigma_A, tau_A, theta, ...
              false);
solver_A.Update(S, Ws, Ps, Is);          

fprintf('Building flow solver\n');
solver_F = SolverFlow;
solver_F.Init(D, K, G_img, W_img, H_img, mask_atlas, ...                      
              w_img, h_img, ...
              lambda_F, sigma_F, tau_F, theta, ...
              false);

err_flow  = [];
err_atlas = [];

err_ds = [];
err_as = [];
err_ss = [];
err_ws = [];

for viewpoint = 1 : outer_loop
    fprintf('OUTER LOOP: %d\n', viewpoint);
        
    %% Optimize Optical flow    
    for iter_flow = 1 : inner_loop_flow
        compute_error = mod(iter_flow, log_interval) == 1;
        err_d = 0;
        err_w = 0;
        rate = 0;
        for k = 1 : n
            tic;
            fprintf('Flow estimation: viewpoint%d\n', k);
            solver_F.Update(A, S, Ps{k}, Is{k});
            [Us{k}, U_bars{k}, p_Us{k}, q_Us{k}, rate_k] ...
                = solver_F.Iterate(Us{k}, U_bars{k}, p_Us{k}, q_Us{k});
            rate = max(rate, rate_k);            
            if compute_error
                [err, err_dk, err_wk] = solver_F.ComputeEnergy(Us{k});
                err_d = err_d + err_dk;
                err_w = err_w + err_wk;
            end
            toc;
        end
        fprintf('Iteration %d: Flow change rate: %f\n', iter_flow, rate);           
        if compute_error
            err_flow = [err_flow, err_d + err_w/lambda_F];
            err_ws   = [err_ws, err_w];
            h3 = figure(3);            
            title('Error flow');
            yyaxis left; plot(err_flow); 
            yyaxis right; plot(err_ws);
            legend('total', 'flow'); drawnow;
            savefig(h3, sprintf('%s/error-flow.fig', output_path));
        end
    end
    for k = 1 : n
        Ws{k} = ConvertFlowVec2Mat(W_img, H_img, Us{k});
    end
    
    %% Optimize Atlas
    for iter_atlas = 1 : inner_loop_atlas
        compute_error = mod(iter_atlas, log_interval) == 1;

        % Optimize Shading
        tic;        
        solver_S.Update(A, Ws, Ps, Is);
        [S, S_bar, p_S, q_Ss, rate_S] = solver_S.Iterate(S, S_bar, p_S, q_Ss);
        fprintf('Iteration %d: S change rate: %f\n', iter_atlas, rate_S);        
        if compute_error
            [err, err_d, err_s] = solver_S.ComputeEnergy(S);
        end
        toc;
        im_S = solver_S.Atlas(S);

        % Optimize Albedo
        tic;
        solver_A.Update(S, Ws, Ps, Is);
        [A, A_bar, p_A, q_As, rate_A] = solver_A.Iterate(A, A_bar, p_A, q_As);
        fprintf('Iteration %d: A change rate: %f\n', iter_atlas, rate_A);
        if compute_error
            [err, err_d, err_a] = solver_A.ComputeEnergy(A);
        end
        toc;
        im_A = solver_A.Atlas(A);

        % Display and output results
        figure(1); subplot(1, 2, 1);
        imshow(im_S); drawnow;    

        figure(1); subplot(1, 2, 2);
        imshow(im_A); drawnow;
        
        if compute_error
            err_atlas = [err_atlas, err_d + err_a / lambda_A + err_s / lambda_S];
            err_ds = [err_ds, err_d];
            err_as = [err_as, err_a];
            err_ss = [err_ss, err_s];             

            %% Primal energy
            x = 1:size(err_atlas, 2);
            h2 = figure(2);
            title('Error Atlas'); 
            yyaxis left;
            plot(x, err_atlas);
            yyaxis right;
            plot(x, err_ds, x, err_as/lambda_A, x, err_ss/lambda_S); 
            legend('total', 'data', 'albedo', 'shading');
            drawnow;
            savefig(h2, sprintf('%s/error-atlas.fig', output_path));

            %% Atlas images
            imwrite(im_S, sprintf('%s/shading%d-%d.png', output_path, ...
                                  viewpoint, iter_atlas));
            imwrite(im_A, sprintf('%s/albedo%d-%d.png', output_path, ...
                                  viewpoint, iter_atlas));
            imwrite(im_S.*im_A, sprintf('%s/texture%d-%d.png', output_path, ...
                                  viewpoint, iter_atlas));

            save(sprintf('%s/error.mat', output_path), ...
                'err_flow', 'err_atlas', ...
                'err_ds', 'err_as', 'err_ss', 'err_ws');
        end
    end
end
end