function K = OperatorGradient(w, h, mask)

Di_x = [1:h , 2:h];
Dj_x = [1:h , 1:h-1];
Dv_x = [0 , 1*ones(1,h-1) , -ones(1,h-1)];

D_x = sparse(Di_x, Dj_x, Dv_x);
D_x = kron(speye(w),D_x);

Di_y = [h+1:h*w , h+1:h*w];
Dj_y = [h+1:h*w , 1:h*(w-1)];
Dv_y = [ones(1,h*(w-1)), -ones(1,h*(w-1))];
D_y = sparse(Di_y, Dj_y, Dv_y);

K = [D_x; D_y];

mask_g = K * mask;
mask_gx = mask_g(1:w*h);
mask_gy = mask_g(w*h+1:end);
idx = ((abs(mask_gx) + abs(mask_gy)) == 0);
mask_g = mask .* idx;

K = sparse(1:2*w*h, 1:2*w*h, repmat(mask_g, 2, 1)) * K;
end

