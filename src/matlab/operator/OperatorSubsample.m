function S = OperatorSubsample(w, h, rate)

nl = floor(w / rate) * floor(h / rate);

Si = [1 : nl, nl];
Sj = zeros(nl + 1, 1);
Sj(nl + 1) = w * h;

cols = floor(w / rate);
for i = 1 : cols    
    s = floor(h / rate);
    l = 1 + s * (i - 1);
    r = s + s * (i - 1);
    Sj(l : r) = (1 : rate : h) + (i - 1) * rate * h;
end
Sv = [ones(1, nl), 0];

S = sparse(Si, Sj, Sv);

end

