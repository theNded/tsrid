%% TODO: optimize it with a vectorized version
function K = OperatorGaussian(w, h, window_size, sigma)

k = fspecial('gaussian', window_size, sigma);

n = w * h;
m = window_size * window_size;
window_size_2 = floor(window_size/2); 

% Borders are ignored (at current)
% 1 + ksize_2 * h + ksize_2 : n - ksize_2 * h - ksize_2    
valid_n = n - 2 * window_size_2 * h - 2 * window_size_2;
Ki = zeros(m * valid_n + 1, 1);
Kj = zeros(m * valid_n + 1, 1);
Kv = zeros(m * valid_n + 1, 1);

Ki(m * valid_n + 1) = n;
Kj(m * valid_n + 1) = n;
Kv(m * valid_n + 1) = 0;

for i = 1 : valid_n
    r = i + window_size_2 * h + window_size_2;
    Ki(m * (i-1) + 1 : m * i) = r * ones(1, m);

    for ki = 1 : window_size
        for kj = 1 : window_size
            idx = m * (i-1) + window_size * (ki - 1) + kj;

            Kj(idx) = r + (-window_size_2 + ki - 1) * h + (-window_size_2 + kj - 1);
            Kv(idx) = k(ki, kj);                
        end
    end
end

K = sparse(Ki, Kj, Kv);

end

