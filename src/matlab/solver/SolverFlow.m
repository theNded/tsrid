classdef SolverFlow<handle
    properties
        %% Constant part    
        % atlas variables
        % D: downsampler
        % K: gaussian kernel
        % G: gradient operator
        % W, H: width, height of the atlas
        % mask: mask image of atlas
        DK; G; W; H; WH; mask_atlas;                
        
        % view-related variables
        n,w; h; P; I_l;
        
        %% Changable part:
        % atlas variables
        A, S;
        
        % multiple-view variables: warping, and related masks
        mask_high_res; mask_low_res;
        % store multiple-view matrices and vectors to speed up
        Ms; cs;
        
        % parameters for primal-dual
        lambda; sigma; tau; theta; 
        
        use_timer;
        %% Primal-dual variables:
        % A; A_bar; p; qs;
    end
    
    methods             
        %% Initialize with multiple variables
        function obj = Init(obj, D, K, G, W, H, mask, ...                      
                      w, h, ...
                      lambda, sigma, tau, theta, ...
                      use_timer)
            obj.DK = D * K; obj.G = G; 
            obj.W = W; obj.H = H; obj.mask_atlas = mask;
            obj.WH = W * H;
            obj.DK = D * K;            

            obj.w = w; obj.h = h;
            obj.Ms = cell(3, 1);
            obj.cs = cell(3, 1);            

            obj.lambda = lambda;
            obj.sigma  = sigma;
            obj.tau    = tau;
            obj.theta  = theta;    
            
            obj.use_timer = use_timer;
        end
        
        %% Update other variables, namely Shading and Warping(s)
        function obj = Update(obj, A, S, P, I_l)
            obj.S = S;
            obj.A = A;
            
            atlas = A.*S;      
            I_h = P * atlas;
            
            for j = 1 : 3
                obj.Ms{j} = obj.DK * ...
                    sparse([1:obj.WH, 1:obj.WH], ...
                           1:2*obj.WH, ...
                           obj.G * I_h(:,j));
                obj.cs{j} = I_l(:,j) - obj.DK * I_h(:,j);
            end
            
            obj.mask_high_res = (P * obj.mask_atlas > 0);
            obj.mask_low_res  = (obj.DK * obj.mask_high_res > 0);
        end
        
        %% Optimize major variables
        function [U, U_bar, p, qs, rate] = Iterate(obj, U, U_bar, p, qs)
            U_prev = U;

            % Dual step: 
            if (obj.use_timer)
                fprintf('Dual p: \n');
                tic;    
            end
            p = p + obj.sigma * ([obj.G * sparse(U_bar(1:obj.WH)); ...
                                  obj.G * sparse(U_bar(obj.WH+1:end))]);
            pdenom = max(1,sqrt(p(1:obj.WH,:).^2 + p(obj.WH+1:2*obj.WH,:).^2 ...
                              + p(2*obj.WH+1:3*obj.WH,:).^2 + p(3*obj.WH+1:end,:).^2));
            p = p ./ [pdenom;pdenom;pdenom;pdenom];
            p = repmat(obj.mask_high_res, 4, 1) .* p;
            if (obj.use_timer)
                toc;
            end

            if (obj.use_timer)
                fprintf('Dual q: \n');
                tic;
            end
            for j = 1 : 3
                qs(:,j) = qs(:,j) + obj.sigma * (obj.Ms{j} * U_bar);
                qs(:,j) = obj.lambda / (obj.lambda + obj.sigma) ...
                    * (qs(:,j) - obj.sigma * obj.cs{j});
                qs(:,j) = obj.mask_low_res .* qs(:,j);
            end 
            if (obj.use_timer)
                toc;
            end

            % Primal step:
            if (obj.use_timer)
                fprintf('Primal: \n');   
                tic;    
            end
            U = U - obj.tau * ([obj.G' * sparse(p(1:2*obj.WH));...
                                obj.G' * sparse(p(2*obj.WH+1:end))]);
            U = U - obj.tau * (obj.Ms{1}' * sparse(qs(:,1)) ...
                             + obj.Ms{2}' * sparse(qs(:,2)) ...
                             + obj.Ms{3}' * sparse(qs(:,3)));
            U = repmat(obj.mask_high_res, 2, 1) .* U;       
            if (obj.use_timer)
                toc;
            end
    
            % Extention step:
            if (obj.use_timer)
                fprintf('Extension: \n');   
                tic;
            end
            U_bar = U + obj.theta * (U - U_prev);
            if (obj.use_timer)
                toc;
            end                        
            rate = norm(U - U_prev) / norm(U_prev);
        end
        
        %% Primal energy
        function [err, err_d, err_r] = ComputeEnergy(obj, U)
            % Data term
            err_d = 0;
            for j = 1 : 3
                d = obj.Ms{j} * U - obj.cs{j};
                d = d.*d;
                % 3 channels separated
                err_d = err_d + sum(d, 1); 
            end
            err_d = full(sum(err_d));

            % Regularizor
            r = ([obj.G * sparse(U(1:obj.WH));...
                  obj.G * sparse(U(obj.WH+1:end))]);
            r = sqrt(r(1:obj.WH,:).^2 + r(obj.WH+1:2*obj.WH,:).^2 ...
                   + r(2*obj.WH+1:3*obj.WH,:).^2 + r(3*obj.WH+1:end,:).^2);          
            err_r = full(sum(r));
            
            err = obj.lambda/2 * err_d + err_r;
        end    
        
        %% Output images        
        function Visualize(obj, U)
            figure(3);
            quiver(1:obj.W, 1:obj.H, ...
                flip(reshape(U(1:obj.WH), obj.H, obj.W), 1), ...
                flip(reshape(U(obj.WH+1:end), obj.H, obj.W), 1));
            drawnow;
        end
                
        function img = HighResolutionImg(obj, U, P)
            mat_U = ConvertFlowVec2Mat(obj.W, obj.H, U);
            img = im2uint8(ConvertVec2Mat(2 * obj.w, 2 * obj.h, 3, ...
                           mat_U * P * (obj.A.*obj.S)));
        end
        
        function img = LowResolutionImg(obj, U, P)
            mat_U = ConvertFlowVec2Mat(obj.W, obj.H, U);
            img = im2uint8(ConvertVec2Mat(obj.w, obj.h, 3, ...
                           obj.DK * mat_U * P * (obj.A.*obj.S)));
        end

    end
    
end

