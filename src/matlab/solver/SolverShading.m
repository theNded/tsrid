classdef SolverShading<handle         
    properties
        %% Constant part    
        % atlas variables
        % D: downsampler
        % K: gaussian kernel
        % G: gradient operator
        % W, H: width, height of the atlas
        % mask: mask image of atlas
        DK; G; W; H; WH; mask_atlas;
        
        % multiple-view variables
        n; w; h; 
        
        %% Changable part:        
        % store multiple-view matrices and vectors to speed up
        Ms; cs; mask_views;
        
        % parameters for primal-dual
        lambda; sigma; tau; theta; alpha;
        
        use_timer;
        
        %% Primal-dual variables:
        % S; S_bar; p; qs;
    end
    
    methods             
        %% Initialize with multiple variables
        function obj = Init(obj, D, K, G, W, H, mask, ...                      
                      n, w, h, ...                      
                      lambda, sigma, tau, theta, alpha, ...
                      use_timer)
            obj.DK = D * K; obj.G = G; 
            obj.W = W; obj.H = H; obj.WH = W * H;
            obj.mask_atlas = mask;
            
            obj.n = n; obj.w = w; obj.h = h;
            obj.Ms = cell(n, 3);
            obj.cs = cell(n, 3);
            obj.mask_views = cell(n, 1);

            obj.lambda = lambda;            
            obj.sigma  = sigma;
            obj.tau    = tau;
            obj.theta  = theta;     
            obj.alpha  = alpha;
            
            obj.use_timer = use_timer;
        end
        
        %% Update other variables, namely Shading and Warping(s)
        function obj = Update(obj, A, Ws, Ps, Is)                                    
            diag_A = cell(3, 1);
            for j = 1 : 3
                diag_A{j} = sparse(1:obj.WH, 1:obj.WH, A(:,j));
            end
            
            for k = 1 : obj.n
                % fprintf('SolverShading: update viewpoint %d\n', k);
                DKWP = obj.DK * Ws{k} * Ps{k};
                for j = 1 : 3
                    obj.Ms{k, j} = DKWP * diag_A{j};                                
                    obj.cs{k, j} = sparse(Is{k}(:,j));
                end
                obj.mask_views{k} = ...
                    (DKWP * obj.mask_atlas > 0);
            end
        end
        
        %% Optimize major variables
        function [S, S_bar, p, qs, rate] = Iterate(obj, S, S_bar, p, qs)
            S_prev = S;

            % Dual step: 
            if (obj.use_timer)
                fprintf('Dual p: \n');
                tic;
            end
            p = p + obj.sigma * (obj.G * sparse(S_bar));
            pdenom = max(1, (sqrt(p(1:obj.WH).^2 + p(obj.WH + 1:end).^2) ...
                / (1 + obj.sigma * obj.alpha)));
            p = p ./ ((1 + obj.sigma * obj.alpha) * [pdenom;pdenom]);
            p = repmat(obj.mask_atlas, 2, 1) .* p;
            if (obj.use_timer)
                toc;
            end

            if (obj.use_timer)
                fprintf('Dual q: \n');
                tic;
            end
            for i = 1 : obj.n
                for j = 1 : 3
                    qs{i}(:,j) = qs{i}(:,j) + obj.sigma * (obj.Ms{i,j} * S_bar);
                    qs{i}(:,j) = obj.lambda / (obj.lambda + obj.sigma) ...
                        * (qs{i}(:,j) - obj.sigma * obj.cs{i, j});
                    qs{i}(:,j) = obj.mask_views{i} .* qs{i}(:,j);
                end
            end         
            if (obj.use_timer)
                toc;
            end

            % Primal step:
            if (obj.use_timer)
                fprintf('Primal: \n');   
                tic;    
            end
            S = S - obj.tau * obj.G' * sparse(p);
            for i = 1 : obj.n
                S = S - obj.tau * (obj.Ms{i, 1}' * sparse(qs{i}(:,1)) ...
                                 + obj.Ms{i, 2}' * sparse(qs{i}(:,2)) ...
                                 + obj.Ms{i, 3}' * sparse(qs{i}(:,3)));
            end
            S = obj.mask_atlas .* S;
            if (obj.use_timer)
                toc;
            end
    
            % Extention step:
            if (obj.use_timer)
                fprintf('Extension: \n');   
                tic;
            end
            S_bar = S + obj.theta * (S - S_prev);
            if (obj.use_timer)            
                toc;
            end
                        
            rate = norm(S - S_prev) / norm(S_prev);
        end
        
        %% Primal energy
        function [err, err_d, err_r] = ComputeEnergy(obj, S)
            huber = @(x) ((x.^2/obj.alpha) .* (abs(x) <= obj.alpha)...
                        + (abs(x)-obj.alpha/2) .* (abs(x) > obj.alpha));
        
            % Data term
            err_d = 0;
            for k = 1 : obj.n
                for j = 1 : 3
                    d = obj.Ms{k, j} * S - obj.cs{k, j};                    
                    err_d = err_d + d.*d; 
                end
            end
            err_d = full(sum(err_d));

            % Regularizor
            r = (obj.G * sparse(S));
            r = sum(sqrt(r(1:obj.WH,:).^2 + r(obj.WH+1:end,:).^2), 1);      
            err_r = sum(huber(full(r)));
            
            err = obj.lambda/2 * err_d + err_r;
        end    
        
        %% Output images
        function atlas = Atlas(obj, S)
            atlas = ConvertVec2Mat(obj.W, obj.H, 1, S);
        end
                
        function img = HighResolutionImg(obj, S, A, Ws_i, Ps_i)
            img = im2uint8(ConvertVec2Mat(2 * obj.w, 2 * obj.h, 3, ...
                Ws_i * Ps_i * (S.*A)));
        end
        
        function img = LowResolutionImg(obj, S, A, Ws_i, Ps_i)
            img = im2uint8(ConvertVec2Mat(obj.w, obj.h, 3, ...
                obj.DK * Ws_i * Ps_i * (S.*A)));
        end
    end    
end

