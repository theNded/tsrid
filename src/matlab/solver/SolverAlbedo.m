classdef SolverAlbedo < handle         
    properties
        %% Constant part    
        % atlas variables
        % D: downsampler
        % K: gaussian kernel
        % G: gradient operator
        % W, H: width, height of the atlas
        % mask: mask image of atlas
        DK; G; W; H; WH; mask_atlas;                
        
        % multiple-view variables
        n; w; h; 
        
        %% Changable part:
        % store multiple-view matrices and vectors to speed up
        Ms; cs; mask_views;
        
        % parameters for primal-dual
        lambda; sigma; tau; theta; 
        
        use_timer;
        
        %% Primal-dual variables:
        % A; A_bar; p; qs;
    end
    
    methods             
        %% Initialize with multiple variables
        function obj = Init(obj, D, K, G, ...
                      W, H, mask, ...                      
                      n, w, h, ...
                      lambda, sigma, tau, theta, ...
                      use_timer)
            obj.DK = D * K; obj.G = G; 
            obj.W = W; obj.H = H; obj.WH = W * H;
            obj.mask_atlas = mask;            

            obj.n = n; obj.w = w; obj.h = h;            

            obj.Ms = cell(n, 1);
            obj.cs = cell(n, 1);
            obj.mask_views = cell(n, 1);

            obj.lambda = lambda;
            obj.sigma  = sigma;
            obj.tau    = tau;
            obj.theta  = theta;         
            
            obj.use_timer = use_timer;
        end
        
        %% Update other variables, namely Shading and Warping(s)
        % Ps and Is are too large, its better not to store them
        function obj = Update(obj, S, Ws, Ps, Is)                                    
            for k = 1 : obj.n
                % fprintf('SolverAlbedo: update viewpoint %d\n', k);
                obj.Ms{k} = obj.DK  * Ws{k} * Ps{k} ...
                          * sparse(1:obj.WH, 1:obj.WH, S(:));
    
                obj.cs{k} = sparse(Is{k});   
                obj.mask_views{k} = ...
                    (obj.DK * Ws{k} * Ps{k} * obj.mask_atlas > 0);
            end
        end
        
        %% Optimize major variables
        function [A, A_bar, p, qs, rate] = Iterate(obj, A, A_bar, p, qs)
            A_prev = A;

            % Dual step: 
            if (obj.use_timer)
                fprintf('Dual p: \n');
                tic;
            end
            p = p + obj.sigma * (obj.G * sparse(A_bar));
            pdenom = max(1,sqrt(p(1:obj.WH,:).^2 + p(obj.WH+1:end,:).^2));
            p = p ./ [pdenom;pdenom];   
            p = repmat(obj.mask_atlas, 2, 1) .* p;
            if (obj.use_timer)
                toc;
            end

            if (obj.use_timer)
                fprintf('Dual q: \n');
                tic;
            end
            for i = 1 : obj.n
                qs{i} = qs{i} + obj.sigma * (obj.Ms{i} * sparse(A_bar));
                qs{i} = obj.lambda / (obj.lambda + obj.sigma) ...
                    * (qs{i} - obj.sigma * obj.cs{i}); 
                qs{i} = obj.mask_views{i} .* qs{i};
            end
            if (obj.use_timer)
                toc;
            end

            % Primal step:
            if (obj.use_timer)
                fprintf('Primal: \n');   
                tic;    
            end
            A = A - obj.tau * (obj.G' * sparse(p));
            for i = 1 : obj.n
                A = A - obj.tau * (obj.Ms{i}' * sparse(qs{i}));
            end
            A = obj.mask_atlas .* A;
            if (obj.use_timer)
                toc;
            end
    
            % Extention step:
            if (obj.use_timer)
                fprintf('Extension: \n');   
                tic;
            end
            A_bar = A + obj.theta * (A - A_prev);
            if (obj.use_timer)
                toc;
            end
                        
            rate = norm(A - A_prev) / norm(A_prev);            
        end
        
        %% Primal energy
        function [err, err_d, err_r] = ComputeEnergy(obj, A)
            % Data term
            err_d = 0;
            for k = 1 : obj.n
                d = obj.Ms{k} * A - obj.cs{k};
                d = d.*d;
                % 3 channels separated
                err_d = err_d + sum(d, 1); 
            end
            err_d = full(sum(err_d));

            % Regularizor
            r = (obj.G * sparse(A));
            err_r = sum(sqrt(r(1:obj.WH,:).^2 + r(obj.WH+1:end,:).^2), 1);      
            err_r = full(sum(err_r));
            
            err = obj.lambda/2 * err_d + err_r;
        end    
        
        %% Output images
        function atlas = Atlas(obj, A)
            atlas = ConvertVec2Mat(obj.W, obj.H, 3, A);
        end
                
        function img = HighResolutionImg(obj, A, S, Ws_i, Ps_i)
            img = im2uint8(ConvertVec2Mat(2 * obj.w, 2 * obj.h, 3, ...
                Ws_i * Ps_i * (A.*S)));
        end
        
        function img = LowResolutionImg(obj, A, S, Ws_i, Ps_i)
            img = im2uint8(ConvertVec2Mat(obj.w, obj.h, 3, ...
                obj.DK * Ws_i * Ps_i * (A.*S)));
        end

    end
    
end

