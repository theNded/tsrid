% @param U: 2wh x 1
function I = VisualizeFlow(W, H, U)

[ang, mag] = cart2pol(U(1:W*H), U(W*H+1:end));

ang = (ang + pi) / (2 * pi);
mag = mag / abs(max(mag));

hsv = zeros(W*H, 3);
hsv(:,1) = ang;
hsv(:,2) = 1.0;
hsv(:,3) = mag;

I = hsv2rgb(hsv);
I = ConvertVec2Mat(W, H, 3, I);

end

