dataset_path = '../../../data/Buddha';

W_atlas = 4096;
H_atlas = 4096;

W_img = 5616;
H_img = 3744;

str = sprintf('%s/texture.png', dataset_path);
atlas = im2double(imread(str));
atlas = ConvertMat2Vec(W_atlas, H_atlas, 3, atlas);

for iter = 1 : 91
fprintf('Loading viewpoint %d\n', iter);

str = sprintf('%s/map/map_%d.txt', dataset_path, iter-1);
P = DecodeProjectionFromFile(str, W_img, H_img, W_atlas, H_atlas);

str = sprintf('%s/image/image_%d.png', dataset_path, iter-1);
I_real = reshape(im2double(imread(str)), W_img*H_img, 3);

I_proj = P*atlas;

figure(1); subplot(1, 3, 1); 
imshow(ConvertVec2Mat(W_img, H_img, 3, I_proj)); 
drawnow;

figure(1); subplot(1, 3, 2); 
imshow(ConvertVec2Mat(W_img, H_img, 3, I_real)); 
drawnow;

figure(1); subplot(1, 3, 3); 
imshow(ConvertVec2Mat(W_img, H_img, 3, abs(I_real - I_proj))); 
drawnow;

end
% D = OperatorSubsample(W_img, H_img, 2);
% K = OperatorGaussian(W_img, H_img, 3, sqrt(0.1));
% M{iter} = D * K * Ps{iter};
% 
% tic;
% atlas = M{iter}' * Is{iter};
% imshow(ConvertVec2Mat(W_atlas, H_atlas, 3, atlas));
% toc;