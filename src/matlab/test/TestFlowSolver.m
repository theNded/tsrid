dataset_path = '../../../data/FaceSmallerSize';
output_path = 'result/Test';

addpath('../converter');
addpath('../operator');
addpath('../projection');
addpath('../solver');
addpath('../util');

%% Resolutions
W_atlas = 256; 
H_atlas = 192;
r = 2;
w_atlas = W_atlas / r;
h_atlas = H_atlas / r;

W_img = W_atlas;
H_img = H_atlas;
w_img = w_atlas;
h_img = h_atlas;

%% Iterations
inner_loop_flow  = 600;

%% Parameters
% E = E_d + (1/lamda_A) E_A + (1/lambda_S) E_S + (1/lambda_F) E_F
lambda_A = 1.5;
%lambda_A = 1.2;
lambda_S = 1;
lambda_F = 2000;

sigma_A = 0.1;
tau_A   = 0.1;
sigma_S = 0.1;
tau_S   = 0.1;
sigma_F = 0.05;
tau_F   = 0.05;
theta = 1;
alpha = 0.1;


%% Prepare data
n = 89;

% Projection matrices high-res-atlas -> high-res-img
Ps = cell(n, 1);
Is = cell(n, 1); 

% Albedo
str = sprintf('%s/InitTexture-diffuse-r.png', dataset_path);
A = im2double(imread(str));%randn(W*H, 3);
A = ConvertMat2Vec(W_atlas, H_atlas, 3, A);

% Shading
str = sprintf('%s/InitTexture-diffuse-s.png', dataset_path);
S = im2double(imread(str));%randn(W*H, 1);
S = S(:);

% Optical Flow
Us   = cell(n, 1);
U_bars = cell(n, 1);
p_Us = cell(n, 1);
q_Us = cell(n, 1);

Ws   = cell(n, 1);

fprintf('Initializing\n');

for iter = 1 : n
    fprintf('Loading projection %d ...\n', iter);    
    str = sprintf('%s/Projection/Frame000/Projection_%03d.mat', dataset_path, iter);
    load (str);
    Ps{iter} = TheProjection.m_ProjectionData;
    PsNormalizer = sparse(1:W_img*H_img, 1:W_atlas*H_atlas, 1./sum(Ps{iter}, 2));
    Ps{iter} = PsNormalizer * Ps{iter};

    fprintf('Loading image %d ...\n', iter);
    str = sprintf('%s/Frame000/Image%d.png', dataset_path, iter);
    Is{iter} = reshape(im2double(imread(str)), w_img*h_img, 3);

    % Optical Flow
    Us{iter} = zeros(2*W_img*H_img,1);    
    U_bars{iter} = zeros(2*W_img*H_img, 1);
    p_Us{iter} = zeros(4*W_img*H_img, 1);
    q_Us{iter} = zeros(w_img*h_img, 3);
    Ws{iter} = ConvertFlowVec2Mat(W_img, H_img, Us{iter});
end

%% Const variables
D = OperatorSubsample(W_img, H_img, 2);
K = OperatorGaussian(W_img, H_img, 3, sqrt(0.1));

str = sprintf('%s/InitTexture-mask.png', dataset_path);
mask_atlas = imread(str) > 0;
mask_atlas = mask_atlas(:,:,1);
mask_atlas = mask_atlas(:);

G_atlas = OperatorGradient(W_atlas, H_atlas, mask_atlas);

mask_img = ones(H_img*W_img,1);
G_img = OperatorGradient(W_img, H_img, mask_img);

huber = @(x) ((x.^2/alpha) .* (abs(x) <= alpha)...
            + (abs(x)-alpha/2) .* (abs(x) > alpha));

%% Initialize optimizers
solver_F = SolverFlow;
solver_F.Init(D, K, G_img, W_img, H_img, mask_atlas, ...                      
              w_img, h_img, ...
              lambda_F, sigma_F, tau_F, theta, ...
              false);
                     
%% Optimize Optical flow    
for k = 1 : n
    fprintf('Viewpoint %d\n', k);
    
    err_flow  = [];
    err_ds = [];
    err_ws = [];
    for iter_flow = 1 : inner_loop_flow
        Ih = Ps{k}*(A.*S);
        solver_F.Update(A, S, Ps{k}, Is{k});

        [Us{k}, U_bars{k}, p_Us{k}, q_Us{k}, rate] ...
            = solver_F.Iterate(Us{k}, U_bars{k}, p_Us{k}, q_Us{k});
        fprintf('Iteration %d: Flow change rate: %f\n', iter_flow, rate);           
        
        [err, err_dk, err_wk] = solver_F.ComputeEnergy(Us{k});

        figure(1);
        subplot(1, 2, 1); 
        imshow(VisualizeFlow(W_img, H_img, Us{k}));
        drawnow;
        
        figure(1);
        subplot(1, 2, 2); 
        Ws{k} = ConvertFlowVec2Mat(W_img, H_img, Us{k});
        imshow(ConvertVec2Mat(W_img, H_img, 3, Ws{k}*Ih));
        drawnow;
        
        figure(2);
        err_flow = [err_flow, err_dk + err_wk/lambda_F];
        err_ds   = [err_ds, err_dk];
        err_ws   = [err_ws, err_wk];
        
        x = 1:size(err_flow, 2);
        title('Error flow');
        yyaxis left; plot(x, err_flow); 
        yyaxis right; plot(x, err_ws, x, err_ds);
        legend('total', 'flow', 'data'); 
        drawnow;
    end
end
    