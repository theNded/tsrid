dataset_path = '../../../data/FaceSmallerSize';
output_path = 'result/Test';

addpath('../converter');
addpath('../operator');
addpath('../projection');
addpath('../solver');

%% Resolutions
W_atlas = 256; 
H_atlas = 192;
r = 2;
w_atlas = W_atlas / r;
h_atlas = H_atlas / r;

W_img = W_atlas;
H_img = H_atlas;
w_img = w_atlas;
h_img = h_atlas;

%% Iterations
inner_loop_atlas = 400;

%% Parameters
% E = E_d + (1/lamda_A) E_A + (1/lambda_S) E_S + (1/lambda_F) E_F
lambda_A = 1.5;
%lambda_A = 1.2;
lambda_S = 1;
lambda_F = 2000;

sigma_A = 0.1;
tau_A   = 0.1;
sigma_S = 0.1;
tau_S   = 0.1;
sigma_F = 0.05;
tau_F   = 0.05;
theta = 1;
alpha = 0.1;


%% Prepare data
n = 89;

% Projection matrices high-res-atlas -> high-res-img
Ps = cell(n, 1);
Is = cell(n, 1); 

% Albedo
str = sprintf('%s/InitTexture-diffuse-r.png', dataset_path);
A = im2double(imread(str));%randn(W*H, 3);
A = ConvertMat2Vec(W_atlas, H_atlas, 3, A);

% Shading
str = sprintf('%s/InitTexture-diffuse-s.png', dataset_path);
S = im2double(imread(str));%randn(W*H, 1);
S = S(:);
S_bar = S;
p_S  = zeros(2*W_atlas*H_atlas, 1);
q_Ss = cell(n, 1);

% Optical Flow
Us   = cell(n, 1);
Ws   = cell(n, 1);

fprintf('Initializing\n');

for iter = 1 : n
    fprintf('Loading projection %d ...\n', iter);    
    str = sprintf('%s/Projection/Frame000/Projection_%03d.mat', dataset_path, iter);
    load (str);
    Ps{iter} = TheProjection.m_ProjectionData;
    PsNormalizer = sparse(1:W_img*H_img, 1:W_atlas*H_atlas, 1./sum(Ps{iter}, 2));
    Ps{iter} = PsNormalizer * Ps{iter};

    fprintf('Loading image %d ...\n', iter);
    str = sprintf('%s/Frame000/Image%d.png', dataset_path, iter);
    Is{iter} = reshape(im2double(imread(str)), w_img*h_img, 3);
        
    % Shading
    q_Ss{iter} = zeros(w_img*h_img, 3);
    
    % Optical Flow
    Us{iter} = zeros(2*W_img*H_img,1);    
    Ws{iter} = ConvertFlowVec2Mat(W_img, H_img, Us{iter});
end

%% Const variables
D = OperatorSubsample(W_img, H_img, 2);
K = OperatorGaussian(W_img, H_img, 3, sqrt(0.1));

str = sprintf('%s/InitTexture-mask.png', dataset_path);
mask_atlas = imread(str) > 0;
mask_atlas = mask_atlas(:,:,1);
mask_atlas = mask_atlas(:);

G_atlas = OperatorGradient(W_atlas, H_atlas, mask_atlas);

mask_img = ones(H_img*W_img,1);
G_img = OperatorGradient(W_img, H_img, mask_img);

huber = @(x) ((x.^2/alpha) .* (abs(x) <= alpha)...
            + (abs(x)-alpha/2) .* (abs(x) > alpha));

%% Initialize optimizers
solver_S = SolverShading();
solver_S.Init(D, K, G_atlas, W_atlas, H_atlas, mask_atlas, ...                      
              n, w_img, h_img, ...              
              lambda_S, sigma_S, tau_S, theta, alpha, ...
              false);
solver_S.Update(A, Ws, Ps, Is);          

err_atlas = [];
err_ds = [];
err_ss = [];
err_ws = [];


%% Optimize Atlas
for iter_atlas = 1 : inner_loop_atlas

    % Optimize Shading
    solver_S.Update(A, Ws, Ps, Is);
    [S, S_bar, p_S, q_Ss, rate_S] = solver_S.Iterate(S, S_bar, p_S, q_Ss);
    fprintf('Iteration %d: S change rate: %f\n', iter_atlas, rate_S);        
    [err, err_d, err_s] = solver_S.ComputeEnergy(S);
    im_S = solver_S.Atlas(S);

    % Display and output results
    figure(1); 
    imshow(im_S); drawnow;    

    err_atlas = [err_atlas, err_d + err_s / lambda_S];
    err_ds = [err_ds, err_d];
    err_ss = [err_ss, err_s];             

    %% Primal energy
    x = 1:size(err_atlas, 2);
    figure(2);
    title('Error Atlas'); 
    yyaxis left;
    plot(x, err_atlas);
    yyaxis right;
    plot(x, err_ds, x, err_ss/lambda_S); 
    legend('total', 'data', 'shading');
    drawnow;
end