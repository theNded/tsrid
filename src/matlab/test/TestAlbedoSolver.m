dataset_path = '../../../data/FaceSmallerSize';
output_path = 'result/Test';

addpath('../converter');
addpath('../operator');
addpath('../projection');
addpath('../solver');

%% Resolutions
W_atlas = 256; 
H_atlas = 192;
r = 2;
w_atlas = W_atlas / r;
h_atlas = H_atlas / r;

W_img = W_atlas;
H_img = H_atlas;
w_img = w_atlas;
h_img = h_atlas;

%% Iterations
outer_loop = 5;
inner_loop_flow  = 2;
inner_loop_atlas = 400;

%% Parameters
% E = E_d + (1/lamda_A) E_A + (1/lambda_S) E_S + (1/lambda_F) E_F
lambda_A = 1.5;
%lambda_A = 1.2;
lambda_S = 1;
lambda_F = 2000;

sigma_A = 0.1;
tau_A   = 0.1;
sigma_S = 0.1;
tau_S   = 0.1;
sigma_F = 0.05;
tau_F   = 0.05;
theta = 1;
alpha = 0.1;


%% Prepare data
n = 89;

% Projection matrices high-res-atlas -> high-res-img
Ps = cell(n, 1);
Is = cell(n, 1); 

% Albedo
str = sprintf('%s/InitTexture-diffuse-r.png', dataset_path);
A = im2double(imread(str));%randn(W*H, 3);
A = ConvertMat2Vec(W_atlas, H_atlas, 3, A);
A_bar = A;
p_A  = zeros(2*W_atlas*H_atlas, 3);
q_As = cell(n, 1);

% Shading
str = sprintf('%s/InitTexture-diffuse-s.png', dataset_path);
S = im2double(imread(str));%randn(W*H, 1);
S = S(:);

% Optical Flow
Us   = cell(n, 1);
Ws   = cell(n, 1);

fprintf('Initializing\n');
for iter = 1 : n
    fprintf('Loading projection %d ...\n', iter);    
    str = sprintf('%s/Projection/Frame000/Projection_%03d.mat', dataset_path, iter);
    load (str);
    Ps{iter} = TheProjection.m_ProjectionData;
    PsNormalizer = sparse(1:W_img*H_img, 1:W_atlas*H_atlas, 1./sum(Ps{iter}, 2));
    Ps{iter} = PsNormalizer * Ps{iter};

    fprintf('Loading image %d ...\n', iter);
    str = sprintf('%s/Frame000/Image%d.png', dataset_path, iter);
    Is{iter} = reshape(im2double(imread(str)), w_img*h_img, 3);
    
    % Albedo
    q_As{iter} = zeros(w_img*h_img, 3);
    
    % Optical Flow
    Us{iter} = zeros(2*W_img*H_img,1);    
    Ws{iter} = ConvertFlowVec2Mat(W_img, H_img, Us{iter});
end

%% Const variables
D = OperatorSubsample(W_img, H_img, 2);
K = OperatorGaussian(W_img, H_img, 3, sqrt(0.1));

str = sprintf('%s/InitTexture-mask.png', dataset_path);
mask_atlas = imread(str) > 0;
mask_atlas = mask_atlas(:,:,1);
mask_atlas = mask_atlas(:);

G_atlas = OperatorGradient(W_atlas, H_atlas, mask_atlas);

mask_img = ones(H_img*W_img,1);
G_img = OperatorGradient(W_img, H_img, mask_img);

huber = @(x) ((x.^2/alpha) .* (abs(x) <= alpha)...
            + (abs(x)-alpha/2) .* (abs(x) > alpha));

%% Initialize optimizers
solver_A = SolverAlbedo();
solver_A.Init(D, K, G_atlas, W_atlas, H_atlas, mask_atlas, ...                      
              n, w_img, h_img, ...              
              lambda_A, sigma_A, tau_A, theta, ...
              false);
solver_A.Update(S, Ws, Ps, Is);          
              
err_atlas = [];
err_ds = [];
err_as = [];
err_ws = [];


%% Optimize Atlas
for iter_atlas = 1 : inner_loop_atlas
    % Optimize Albedo
    solver_A.Update(S, Ws, Ps, Is);
    [A, A_bar, p_A, q_As, rate_A] = solver_A.Iterate(A, A_bar, p_A, q_As);
    fprintf('Iteration %d: A change rate: %f\n', iter_atlas, rate_A);
    [err, err_d, err_a] = solver_A.ComputeEnergy(A);
    im_A = solver_A.Atlas(A);

    % Display and output results
    figure(1); 
    imshow(im_A); drawnow;

    err_atlas = [err_atlas, err_d + err_a / lambda_A];
    err_ds = [err_ds, err_d];
    err_as = [err_as, err_a];

    %% Primal energy
    x = 1:size(err_atlas, 2);
    figure(2);
    title('Error Atlas'); 
    yyaxis left;
    plot(x, err_atlas);
    yyaxis right;
    plot(x, err_ds, x, err_as/lambda_A); 
    legend('total', 'data', 'albedo');
    drawnow;
end
