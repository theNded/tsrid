dataset_path = '../../../data/Face';

W_atlas = 2560;
H_atlas = 1920;

w_img = 1280;
h_img = 960;

W_img = 2560;
H_img = 1920;

for iter = 1 : 89
str = sprintf('%s/map/map_%d.txt', dataset_path, iter-1);
P = DecodeProjectionFromFile(str, W_img, H_img, W_atlas, H_atlas);

str = sprintf('%s/InitTexture-diffuse-r.png', dataset_path);
A = im2double(imread(str));%randn(W*H, 3);
A = ConvertMat2Vec(W_atlas, H_atlas, 3, A);

% Shading
str = sprintf('%s/InitTexture-diffuse-s.png', dataset_path);
S = im2double(imread(str));%randn(W*H, 1);
S = S(:);

fprintf('Loading image %d ...\n', iter);
str = sprintf('%s/Frame000/Image%d.png', dataset_path, iter);
I_real = reshape(im2double(imread(str)), w_img*h_img, 3);

I_proj = P*(A.*S);

figure(1); subplot(1, 2, 1); 
imshow(ConvertVec2Mat(W_img, H_img, 3, I_proj)); 
drawnow;

figure(1); subplot(1, 2, 2); 
imshow(ConvertVec2Mat(W_atlas, H_atlas, 3, P'*I_proj)); 
drawnow;

end
% D = OperatorSubsample(W_img, H_img, 2);
% K = OperatorGaussian(W_img, H_img, 3, sqrt(0.1));
% M{iter} = D * K * Ps{iter};
% 
% tic;
% atlas = M{iter}' * Is{iter};
% imshow(ConvertVec2Mat(W_atlas, H_atlas, 3, atlas));
% toc;