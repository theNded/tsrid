## For most datasets, use RunDataset.m. 
   Modify Config files in config/ folder accordingly:
   - Select IMAGES in section. III. By default every image is loaded.
   - Set LOOPS in section. IV. By default iteration numbers are large.
   Then select the desired dataset in the 'switch' expression in RunDataset.m

## For Torus dataset, use RunTorus.m, as the image indexing is different, 
   and PSNR computations are available on the synthetic dataset.

## For FaceLowRes dataset, use RunFaceLowRes.m, 
   as the projection matrix loading is slightly different.