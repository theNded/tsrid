function mat = ConvertVec2Mat(w, h, c, vec)

mat = zeros(h, w, c);
for i = 1 : c
    mat(:,:,i) = reshape(vec(:,i), h, w);
end

end

