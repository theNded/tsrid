% @param w: width 
% @param h: height
% @param U: vectorized flow field 2wh x 1
function [ mat_flow ] = ConvertFlowVec2Mat(w, h, vec_flow)  

idx_ConvertVec2Mat = @(i) ([mod(i-1, h) + 1, floor((i-1) / h) + 1]);
idx_ConvertMat2Vec = @(i, j) ((j-1)*h + i);
 
%% Border
uv_old = idx_ConvertVec2Mat((1:w*h)');
du = vec_flow(1:w*h);
dv = vec_flow(w*h+1:end);
uv_new = [uv_old(:,1) + du, uv_old(:,2) + dv];
valid_idx = uv_new(:,1) > 1 & uv_new(:,1) < h ...
          & uv_new(:,2) > 1 & uv_new(:,2) < w;

uv_old = uv_old(valid_idx,:);
uv_new = uv_new(valid_idx,:);

%% Interpolation
% floor
uf = floor(uv_new(:,1));
vf = floor(uv_new(:,2));
% ceil
uc = uf + 1;
vc = vf + 1;
% interpolation ratio
ru = uv_new(:,1) - uf;
rv = uv_new(:,2) - vf;
 
%% Assignment
idx_old = idx_ConvertMat2Vec(uv_old(:,1), uv_old(:,2));
Wi = [idx_old; idx_old; idx_old; idx_old];
Wj = [idx_ConvertMat2Vec(uf, vf); idx_ConvertMat2Vec(uf, vc); ...
      idx_ConvertMat2Vec(uc, vf); idx_ConvertMat2Vec(uc, vc)];
Wv = [(1-ru).*(1-rv); (1-ru).*rv; ru.*(1-rv); ru.*rv];
mat_flow = sparse(Wi, Wj, Wv, w*h, w*h); 
 
end
