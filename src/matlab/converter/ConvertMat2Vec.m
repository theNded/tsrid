function vec = ConvertMat2Vec(w, h, c, mat)

vec = zeros(w*h, c);
for i = 1 : c
    vec(:,i) = reshape(mat(:,:,i), w*h, 1);
end

end

